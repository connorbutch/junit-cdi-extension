# junit-cdi-extension

This project is a JUnit (5) extension.  It allows the injection of parameters based on cdi.  This offers features not available with weld for junit.