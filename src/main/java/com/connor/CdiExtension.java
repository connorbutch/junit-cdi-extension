package com.connor;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class CdiExtension implements ParameterResolver {

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		return true;
	}

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		
		//NOTE: this could have some overhead, but extensions are supposed to be stateless, so this is the way to do this without storing state
		//if starting cdi container for each injected field (shouldn't happen often) becomes an overhead, then we can likely move the container to 
		//a singleton, then implement AfterEachCallback here to close the container at the end
		
		try(WeldContainer cdiContainer = new Weld().initialize()){
			return cdiContainer.select(parameterContext.getParameter().getType(), parameterContext.getParameter().getAnnotations()).get();
		}catch(Exception e) {
			throw new ParameterResolutionException("Couldn't resolve parameter", e);
		}
	}
}